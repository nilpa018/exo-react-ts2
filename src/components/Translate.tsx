
import { useState } from 'react';
import { TRANSLATE_BUTTON, TRANSLATE_INPUT, INSTRUCTIONS, TRANSLATION_LANG } from '../utils/Constants';
import './translate.css';

const Translate = () => {
    const [value, setValue] = useState('');
    const [source, setSource] = useState('FR');
    const [language, setLanguage] = useState(TRANSLATION_LANG);

    const handleChange = (e) => {
        setValue(e.target.value);
    }
    const handleChangeLang = (e) => {
        setLanguage(e.target.value);
    }
    const handleChangeSource = (e) => {
        setSource(e.target.value);
    }
    const handleSubmit = async () => {
        // TODO: Faire l'appel API avec les bonnes informations et afficher le résultat de la traduction
    }


    return (
        <>
            <p>{INSTRUCTIONS.translate}</p>
            <div className="translate">
                <input type="text" placeholder="Source" size={2} maxLength={2} value={source} onChange={handleChangeSource} />
                <input className="valueTexte" type="text" placeholder={TRANSLATE_INPUT} value={value} onChange={handleChange} />
                <input type="text" placeholder="Lang" size={2} maxLength={2} value={language} onChange={handleChangeLang} />
                <button type="button" onClick={handleSubmit} disabled={!value || !language}>{TRANSLATE_BUTTON}</button>
            </div >
            <div className="translate-response">
                <p className="translate-title">TRADUCTION</p>
                {/* // TODO: Mettre ici le resultat de la traduction */}
            </div >
        </>
    )

}

export default Translate