export const EXERCICE_TITLE = "Traduit Moi !";
export const EXERCICE_VERSION = 2;
export const TRANSLATE_BUTTON = "Translate";
export const TRANSLATE_INPUT = "Texte";
export const TRANSLATION_LANG = "EN";

export const INSTRUCTIONS = {
  exercice: `Le but de cet exercice est d'ajouter un appel API qui permettra d'effectuer des traductions au click, 
    dans cette seconde étape, vous devez également typer la fonction qui fait appel à l'API`,
  translate: "Veuillez entrer un texte à traduire et cliquer sur Translate",
};
