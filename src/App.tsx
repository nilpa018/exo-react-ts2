import { EXERCICE_TITLE, EXERCICE_VERSION, INSTRUCTIONS } from './utils/Constants';
import Translate from './components/Translate';
import typescript from '../src/assets/typescript.jpg';
import './app.css'

const App = () => {

  return (
    <>
      <img className='typescript_img' src={typescript}></img>
      <h1>{EXERCICE_TITLE} <span className="version">(V{EXERCICE_VERSION}/3)</span></h1>
      <div className="card">
        <Translate />
      </div>
      <p className='exercice'>
        {INSTRUCTIONS.exercice}
      </p>
    </>
  )
}

export default App
