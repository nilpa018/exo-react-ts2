# Exercice React + TypeScript : Création d'une application de traduction v2

## Objectif

Le but de cet exercice 2 est d'ajouter un appel API qui permettra d'effectuer des traductions au click, nous rajoutons la possiblilité de changer la langue de saisie et vous devez également typer ce qui peut l'être.

## Prérequis

Assurez-vous d'avoir Node.js installé sur votre machine. 
Vous enregistrer gratuitement sur le site de Systran afin d'obtenir une clé API limitée.

## Étapes

### Étape 1: Initialisation du projet React

1. Clonez le repository depuis GitLab :

   ```bash
   git clone https://gitlab.com/nilpa018/exo-react-ts2.git
   ```

2. Accédez au répertoire du projet :

   ```bash
   cd exo-react-ts2
   ```

3. Lancez l'installation des modules et construction du dossier public :

   ```bash
   npm install
   ```

4. Lancez l'application pour vous assurer que tout fonctionne correctement :
   ```bash
   npm run dev
   ```

### Étape 2: Contenu du projet

Le projet est constitué comme ceci :

Dossier src

- App.tsx: Le composant principale
- app.css: La feuille de style du composant App.tsx   

Sous dossier API  

- ApiCall.tsx Le composant qui fait la requete axios vers l'Api, il retourne une Promise. 

Sous dossier assets: il contient l'image TypeScript dans le composant App.

Sous dossier components

- Translate.tsx: Le composant contient les instructions ainsi qu'un input et un bouton.
- translate.css: La feuille de style du composant Translate.tsx

Sous dossier utils

- Constant.ts: Le composant contient les constantes de l'application

### Étape 3: Objectif de l'exercice

Vous devez ajouter un appel Api vers Translate API de Systran (voir lien dans la rubrique Ressources). Afin de gagner du temps, la plupart du graphisme est déjà fournis. Vous devrez également typer tout ce qui peut l'être.   

Vous pouvez si vous le souhaitez utiliser l'Union Types afin de rajouter des valeurs de votre choix sans modifier l'interface utilisateur.

**Dans la version 3 de l'exercice, nous améliorerons notre outil de traduction.**

### Étape 4: Conditions de réussite

Toutes les constantes et fonctions sont typées et reliées avec le fichier Types.ts
Aucun type "any" n'est permis.   
Si plusieurs types sont possibles, il faut les regrouper.   
Le linteur doit être actif et doit passer sans erreurs ni avertissements.  
Lancez le linteur avant de retourner l'exercice pour correction.

Assurez-vous d'inclure le code source complet pour la vérification.

### Étape 5: Vérification

Le candidat devra fournir l'ensemble des fichiers à l'exception des node_modules après avoir complété les tâches.

### Aperçu graphique

Voici une représentation de ce qui est attendu graphiquement, vous avez la possibilité de modifier un peu celui-ci.

![Representation graphique de l'exercice](/images/exo-react-ts2.png)

### Remarques

- Afin de répondre aux meilleurs pratiques, déclarez vos variables de préférences avec const et let.
- Commentez votre code pour expliquer votre démarche si c'est pertinent.

### Ressources

Documentation React : https://reactjs.org/docs/getting-started.html  
Documentation TypeScript : https://www.typescriptlang.org/  
Documentation Systran Translate : https://www.systransoft.com/translate/   
Documentation Technique Translate API : https://docs.systran.net/translateAPI/translation/   

### Soumission

Lorsque vous avez terminé, assurez-vous de fournir le code source du projet pour évaluation soit en l'envoyant au format ZIP sans y inclure les node_modules par [E-mail](mailto:nilpa018@yahoo.fr) soit en l'hébergeant sur un Drive afin de pouvoir télécharger celui-ci.
